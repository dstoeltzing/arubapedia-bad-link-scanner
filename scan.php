<?php
//set_time_limit(18000); // 5 hours (seems to take about 3)

require_once __DIR__.'/vendor/autoload.php';
require_once 'config.php';

use App\BadLinkScanner;

$BadLinkScanner = new BadLinkScanner;
$BadLinkScanner->execute();