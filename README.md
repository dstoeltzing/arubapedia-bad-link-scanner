To install:

1. Pull repo into a folder called 'BadLinkScanner' in your extensions directory
2. run `composer install` from folder
3. Create a new .env file `cp .env.example .env`
4. Update your .env file

To run the scan: `php scan.php`

To optionally send a notification to slack upon completion, add `--notifySlack`