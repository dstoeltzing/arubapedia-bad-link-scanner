<?php

namespace App;

use Illuminate\Database\Capsule\Manager as Capsule;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class BadLinkScanner {

    private $badLinks;
    private $questionableLinks;
    private $badCodes;
    private $totalLinkCount;
    private $processedLinkCount;
    private $notifySlack;

    /**
     * BadLinkScanner constructor.
     */
    function __construct()
    {
        global $argv;

        $this->badCodes = [ "400", "401", "403", "404", "405", "502", "503", "504"];
        $this->notifySlack = in_array('--notifySlack', $argv) ? true : false;

        $capsule = new Capsule;

        $capsule->addConnection([
            'driver'    => 'mysql',
            'host'      => DB_HOST,
            'database'  => DB_DB,
            'username'  => DB_USER,
            'password'  => DB_PASS,
        ]);

        $capsule->setAsGlobal();
        $capsule->bootEloquent();
    }

    /**
     * Run the scan
     */
    public function execute() {
        //$this->postMessageToSlack('Broken link scanner started');
        $matches = $this->findUrls();
        $this->processUrls($matches);
        $this->sendEmailNotification();
        if($this->notifySlack) $this->postMessageToSlack('Broken link scanner complete :thumbsup:');
    }

    /**
     * Search the database for http and https links
     *
     * @return \Illuminate\Support\Collection
     */
    private function findUrls()
    {
        $links = Capsule::table('externallinks')
            ->select(['externallinks.el_to', 'page.page_title'])
            ->join('page', 'page.page_id', '=', 'externallinks.el_from')
            ->where([
                ['externallinks.el_to', 'NOT LIKE', '%mailto%'],
                ['externallinks.el_to', 'NOT LIKE', '%.exe']
            ])

            ->get();
        $this->totalLinkCount = count($links);

        print "URLS FOUND: $this->totalLinkCount\n";
        return $links;
    }

    /**
     * Iterate through links found in database and check them
     *
     * @param \Illuminate\Support\Collection $links
     * @return void
     */
    private function processUrls(\Illuminate\Support\Collection $links)
    {
        $this->processedLinkCount = 1;
        foreach ($links as $link) {
            $url = $link->el_to;
            $resCode = $this->checkURL($url);
            if(in_array($resCode, $this->badCodes))
            {
                $this->badLinks[$link->page_title][] = $url . " ($resCode)";
            } else if($resCode === 0) {
                $this->questionableLinks[$link->page_title][] = $url . " ($resCode)";
            }
            $this->processedLinkCount++;
        }

        print "SCAN COMPLETED\n";
    }

    /**
     * Check to see if an individual link is valid
     *
     * @param $url
     * @return bool
     */
    private function checkURL($url)
    {
        //$this->log->info("Page $this->processedLinkCount/$this->totalLinkCount Checking: $url");
        print "Page $this->processedLinkCount/$this->totalLinkCount Checking: $url\n";

        $ch = curl_init( $url );
        curl_setopt( $ch, CURLOPT_HEADER, true );
        curl_setopt( $ch, CURLOPT_USERAGENT, "Arubapedia" );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch, CURLOPT_TIMEOUT, CURL_TIMEOUT );
        curl_exec( $ch );
        $result = curl_getinfo( $ch );
        curl_close( $ch );
        $resCode = (int)$result['http_code'];

        print "Response Code: $resCode\n";

        return $resCode;
    }

    /**
     * Builds html table to view as a MediaWiki Special Page
     * @return string 
     */
    public function buildSpecialPage() {
        // Commenting this out for now, we probably don't want people to be able to fire this process off willy-nilly
        //$matches = $this->findUrls();
        //$this->processUrls($matches);
        //$body = $this->buildHtmlContent();
        return '';
    }

     /**
     * Builds Html Table Page
     */
    private function buildHtmlContent() 
    {
        global $wgServer;

        if(empty($this->badLinks) && empty($this->questionableLinks)) return;

        $body = '<p>The bad link scanner has completed and found the following invalid URLs in most recent revisions.</p>';

        $body .= '<p><strong>Database:</strong> '.DB_DB.'</p>';
        $body .= '<p><strong>Bad Links: </strong> '.count($this->badLinks).'</p>';
        $body .= '<p><strong>Questionable Links: </strong> '.count($this->questionableLinks).'</p>';

        $body .= '<h4>Bad Links</h4>';
        $body .= '<table style="border:1px solid black; width: 100%">';
        foreach ($this->badLinks as $pageName => $badLinks)
        {
            $body .= '<tr valign="top" style="border:1px solid black">';
            $body .= '<td>'.$pageName.'</td>';
            $body .= '<td><ul>';
            foreach ($badLinks as $badLink)
            {
                $body .= '<li>'.$badLink.'</li>';
            }
            $body .= '</ul></td>';
            $body .= '</tr>';
        }
        $body .= '</table>';

        $body .= '<h4>Questionable Links</h4>';
        $body .= '<table style="border:1px solid gray; width: 100%">';
        foreach ($this->questionableLinks as $pageName => $badLinks)
        {
            $body .= '<tr valign="top" style="border:1px solid black">';
            $body .= '<td>'.$pageName.'</td>';
            $body .= '<td><ul>';
            foreach ($badLinks as $badLink)
            {
                $body .= '<li>'.$badLink.'</li>';
            }
            $body .= '</ul></td>';
            $body .= '</tr>';
        }
        $body .= '</table>';

        return $body;
    }

    /**
     * Send the email notification listing bad links
     */
    private function sendEmailNotification()
    {
        if(empty($this->badLinks)) return;

        $mail = new PHPMailer(true);

        try
        {
            $mail->setFrom(MAIL_FROM, 'Mailer');
            $mail->addAddress(MAIL_TO);
            $mail->addCC(MAIL_CC);

            $body = $this->buildHtmlContent();

            $mail->isHTML(true);
            $mail->Subject = 'Arubapedia bad links found';
            $mail->Body    = $body;

            $mail->send();
        } catch (Exception $e) {
            echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }

    }

    /**
     * Send message to slack #arubapedia channel
     *
     * @param $message
     */
    private function postMessageToSlack($message)
    {
        if(empty($message)) return;

        $webHook = SLACK_HOOK;
        $message = array('payload' => json_encode(array('text' => $message, 'username' => 'arubapediabot')));
        $c = curl_init($webHook);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_POST, true);
        curl_setopt($c, CURLOPT_POSTFIELDS, $message);
        curl_exec($c);
        curl_close($c);
    }

}