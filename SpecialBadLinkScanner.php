<?php

require_once __DIR__.'/vendor/autoload.php';
require_once 'config.php';

use App\BadLinkScanner;
use MediaWiki\MediaWikiServices;

class SpecialBadLinkScanner extends IncludableSpecialPage {
	private $badLinkScanner;
	
	function __construct() {
		parent::__construct( 'BadLinkScanner' );
		$this->badLinkScanner = new BadLinkScanner;
	}

	function execute( $par ) {
		$request = $this->getRequest();
		$output = $this->getOutput();
		$this->setHeaders();

		# Get request data from, e.g.
		$param = $request->getText( 'param' );

		# Do some stuff
		# ...
		$wikitext = $this->badLinkScanner->buildSpecialPage();
		$output->addWikiText( $wikitext );
	}
}

