<?php
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

// DB settings
define('DB_HOST', getenv('DB_HOST'));
define('DB_DB', getenv('DB_DB'));
define('DB_USER', getenv('DB_USER'));
define('DB_PASS', getenv('DB_PASS'));

// Curl settings
define('CURL_TIMEOUT', getenv('CURL_TIMEOUT'));

// Mail settings
define('MAIL_FROM', getenv('MAIL_FROM'));
define('MAIL_TO', getenv('MAIL_TO'));
define('MAIL_CC', getenv('MAIL_CC'));

// Slack settings
define('SLACK_HOOK', getenv('SLACK_HOOK'));
